package main
import (
"fmt"

"github.com/gin-gonic/gin"
"github.com/jinzhu/gorm"
 _ "github.com/jinzhu/gorm/dialects/sqlite"
)
var db *gorm.DB
var err error

type Kelas struct {
 ID uint `json:”id”`
 NamaKelas string `json:"namakelas”`
 Ruangan string `json:”ruangan”`
 Kehadiran []Kehadiran `gorm:"foreignkey:NamaSiswa"`
}

type Kehadiran struct {
 ID uint `json:"id"`
 NamaSiswa string `json:"namasiswa"`
 Telepon string `json:"telepon"`
}

func main() {
 // NOTE: See we’re using = to assign the global var
 // instead of := which would assign it only in this function
 db, err = gorm.Open("sqlite3", "./gorm.db")
 if err != nil {
    fmt.Println(err)
 }
 defer db.Close()

 db.AutoMigrate(&Kelas{}, &Kehadiran{})

 r := gin.Default()
 r.GET("/kelas/", GetSemuakelas)
 r.GET("/kelas/:id", GetKelas)
 r.POST("/kelas", CreateKelas)
 r.GET("/kehadiran/", GetSemuakehadiran)
 r.GET("/kehadiran/:id", GetKehadiran)
 r.POST("/kehadiran", CreateKehadiran)
 r.Run(":8080")
}

func CreateKelas(c *gin.Context) {
	var kelas Kelas
	c.BindJSON(&kelas)

	db.Create(&kelas)
	c.JSON(200, kelas)
}

func CreateKehadiran(c *gin.Context) {
	var kehadiran Kehadiran
	c.BindJSON(&kehadiran)

	db.Create(&kehadiran)
	c.JSON(200, kehadiran)
}

func GetKelas(c *gin.Context) {
 id := c.Params.ByName("id")
 var kelas Kelas
 if err := db.Where("id = ?", id).First(&kelas).Error; err != nil {
    c.AbortWithStatus(404)
    fmt.Println(err)
 } else {
    c.JSON(200, kelas)
 }
}

func GetKehadiran(c *gin.Context) {
 id := c.Params.ByName("id")
 var kehadiran Kehadiran
 if err := db.Where("id = ?", id).First(&kehadiran).Error; err != nil {
    c.AbortWithStatus(404)
    fmt.Println(err)
 } else {
    c.JSON(200, kehadiran)
 }
}

func GetSemuakelas(c *gin.Context) {
 var semuakelas []Kelas
 if err := db.Find(&semuakelas).Error; err != nil {
 c.AbortWithStatus(404)
    fmt.Println(err)
 } else {
    c.JSON(200, semuakelas)
 }
}

func GetSemuakehadiran(c *gin.Context) {
 var semuakehadiran []Kehadiran
 if err := db.Find(&semuakehadiran).Error; err != nil {
 c.AbortWithStatus(404)
    fmt.Println(err)
 } else {
    c.JSON(200, semuakehadiran)
 }
}

// func GetSemuakelas(c *gin.Context) {
	
// 	defer db.Close()
// 	var kelas []Kelas

// 	if err := db.Raw("SELECT * FROM kelass ORDER BY id DESC").Scan(&kelas).Error; err != nil {
// 		c.AbortWithStatus(404)
// 		fmt.Println(err)
// 	}

// 	for i := 0; i < len(kelas); i++ {

// 		var kelas Kelas
// 		var kehadiran Kehadiran

// 		// if err := db.Where("id = ?", kelas[i].IDKelas).Find(&kelas).Error; err != nil {
// 		// 	c.AbortWithStatus(404)
// 		// 	fmt.Println(err)
// 		// }
// 		// collapse
// 		kelas[i].CollabsCount = CountCollabs(kelas[i].ID)
// 		fmt.Println(kelas[i].CollabsCount)
// 		NamaKelas = ""
// 		Ruangan = ""
// 		kelas[i].NamaKelas = namakelas
// 		// symbol for array []
// 		// read all comment in post
// 		if err := db.Where("id_kelas = ?", kelas[i].ID).Find(&kehadiran).Error; err != nil {
// 			c.AbortWithStatus(404)
// 			fmt.Println(err)
// 		}
// 		// if comment not nil
// 		// run this
// 		if len(kehadiran) > 0 {
// 			kelas[i].Kehadiran = kehadiran
// 			for n := 0; n < len(kelas[i].Kehadiran); n++ {
// 				if err := db.Where("id_kelas = ?", kelas[i].Kehadiran[n].IDKelas).First(&kelas).Error; err != nil {
// 					c.AbortWithStatus(404)
// 					fmt.Println(err)
// 				}
// 				kelas[i].Kehadiran[n].NamaKelas = namakelas
// 				fmt.Println(NamaKelas)
// 				fmt.Println(kelas[i].Kehadiran[n].IDKelas)
// 			}
// 		}
// 	}
// 	data := map[string]interface{}{
// 		"kelas": kelas,
// 	}
// 	c.JSON(200, kels)
// }